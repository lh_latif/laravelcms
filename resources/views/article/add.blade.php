@extends('layouts.theme')

@section('title')
    Welcome to Blog
@endsection

@section('content')
    <form action="/article" method="POST">
        <label>Title</label>
        <input type="text" name="title" placeholder="Article Title">
        <label>Content</label>
        <textarea name="content" placeholder="Article Content"></textarea>
        <label>Category</label>
        <select name="category" placeholder="Article Category">
            @foreach ($categories as $category )
            <option value="{{ $category->name }}">{{ $category->name }}</option>
            @endforeach
        </select>
        <input type="submit" value="submit" name="submit" >
        {{ csrf_field() }}
    </form>
@endsection
