<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function add( Request $request ) {
        $category = new Category;
        $category->name = $request->name;
        $category->save();
    }

    public function update ( Request $request ) {
        $category = App\Category::findOrFail($request->id);
        $category->name = $request->name;
        $category->save();
    }

    public function delete ( Request $request ) {
        $category = App\Category::findOrFail($request->id);
        $category->deletes();
    }
}
